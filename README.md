# Seven Sun Downs

_Experimental, Bleeding Edge MMORPG utilizing Ryzom Core._

## License

### Font

SPDX-License-Identifier: [Artistic-1.0](https://spdx.org/licenses/Artistic-1.0.html)

### Logo

SPDX-License-Identifier: [CC-BY-SA-4.0](https://spdx.org/licenses/CC-BY-SA-4.0.html)

### Source Code

SPDX-License-Identifier: [GPL-3.0-or-later](https://spdx.org/licenses/GPL-3.0-or-later.html)
