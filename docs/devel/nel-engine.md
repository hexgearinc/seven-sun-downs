---
title: NeL Engine
---

# NeL Engine

Seven Sun Downs is based on [ryzomcore](https://github.com/ryzom/ryzomcore),
which is the free source code behind the [Ryzom MMORPG](http://ryzom.com). 
NeL (for Nevrax Library) is the game engine on which it runs.
It uses [OpenGL](https://opengl.org) for 3D graphics. 

## Reference

- [NeL Source Code](https://github.com/ryzom/ryzomcore/tree/develop/nel)

- [NeL Sourceforge Project](https://sourceforge.net/projects/nel)

- [NeL: The Software Behind the Next Great MMORPG? (WayBack Machine)](https://web.archive.org/web/20171207212832/http://www.linuxdevcenter.com:80/pub/a/linux/2003/10/16/nel.html)

## See Also

- [Gamekit](https://github.com/gamekit-developers/gamekit/wiki)

- [WorldForge](https://www.worldforge.org)
