---
category: lore
---

> other-worldly; that was another [world](http://ryzom.com/?page=page_planet), they say during the eclipse, it can still be seen.
