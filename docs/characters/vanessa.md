---
category: character
age: 24
role: medic
gender: female
status: living
height: 5'7
weight: 160
hair: brown
eyes: green
faction: icc
---

# Vanessa

_Having worked in a physical rehabilitation for the company,
Vanessa now suddenly finds herself being shipped off on an urgent mission.
Unprepared for the combat and fighting that has erupted around her, she is now
the only surviving member of the crashed research shuttle with any medical training.
Her first field experience was performing the emergency amputation on
[Nathan](../nathan)'s arm, saving his life. When a problem arises in the middle
of a fight, she tends to freezes up and needs to be motivated to help out._
