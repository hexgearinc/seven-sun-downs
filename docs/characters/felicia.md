---
category: character
age: 23
role: combat
gender: female
status: living
height: 5'6
weight: 150 
hair: black
eyes: brown
faction: icc
---

# Felicia

_As childhood friends, [you](../you) and Felicia have a very competitive past.
Graduating at the top of her class, she completed her training a over a year ago now,
and has been serving as a vanguard for only a brief period of time before jumping
at the opportunity to join this mission._
