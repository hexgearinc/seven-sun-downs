---
category: character
age: 35
role: combat
rank: captain
gender: male
status: living
height: 6'3
weight: 250
hair: none
eyes: brown
faction: icc
---

# Captain Tal

_As a retired vanguard, Tal is a seasoned fighter, and experienced captain of
the [Nehemias](../../lexicon/nehemias). While he may be one of the company's oldest vetarans,
he is quite the loose cannon, and has a tendency to go rogue. Recently, he has
been behaving due to the presence of [Miyuki](../miyuki), his adopted daughter.
Captain Tal has a very rough exterior, but hidden underneat is a soft spot for
his daugher. He is always trying to see the big picture rather than focusing on
individual circumstances._
