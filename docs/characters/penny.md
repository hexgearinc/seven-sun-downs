---
category: character
age: 25
role: scientist
gender: female
status: living
height: 5'5
weight: 145
hair: blonde
eyes: blue
faction: icc
---

# Penny

_A child genius, Penny seeks to learn everything she can about the universe.
She is fascinated by the life forms on this new planet. Although, personal
trajedies in her past have left her pondering the meaning of life._
