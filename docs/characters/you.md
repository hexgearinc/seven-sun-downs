---
category: character
status: living
---

# You?

You are the main protagonist of 7 Sun Downs.

_Graduating only second to your childhood friend [Felicia](../felicia),
You were held back during your training due to health concerns.
However, it was because of this, that you were able to befriend
[Nathan](../nathan), who you met during your training._
