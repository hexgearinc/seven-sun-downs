---
category: character
age: 16
role: engineer
gender: female
status: living
height: 5'
weight: 90
hair: black
eyes: brown
faction: icc
---

# Miyuki

_Adopted by [Tal](../tal) after losing her parents, she developed a
love for improvising weapons. Miyuki knows how to craft nearly anything,
although she technically has no official engineering training, much to
[Nathan](../nathan)'s dismay. She has been tagging along with her father
ever since she was adopted, although once she "accidentally" dismantled
several company vessels while on a mission._
