---
category: character
age: 18
role: engineer
nickname: skiff
gender: male
status: living
height: 5'4
weight: 130
hair: brown
eyes: brown
faction: icc
---

# Nathan

_He and [you](../you) became close friends during vanguard training.
Not really prone to fighting, but instead prefers diplomatic solutions. 
Upon losing his arm during the shuttle crash, Nathan now sports a shiny metallic
robotic arm in place of his old limb. He is a highly skilled engineer, however,
he can be very timid at times. Nathan loves his work and often becomes fully
absorbed in every task. This mission is his first field assignment._
